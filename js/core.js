/**
 * Created by Grzegorz on 03.03.14.
 */
(function () {
    $.fn.bootstrapPaginator.defaults.bootstrapMajorVersion = 3;
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
    window.paceOptions = {
        startOnPageLoad: false
    };
    window.PagOpt = {useBootstrapTooltip: true, numberOfPages: 5, alignment: 'center', tooltipTitles: function (type, page) {
        switch (type) {
            case "first":
                return "<span class='glyphicon glyphicon-fast-backward'></span> Idź do pierwszej strony";
            case "prev":
                return "<span class='glyphicon glyphicon-backward'></span> Idź do poprzedniej strony możesz użyć strzałek na klawiaturze";
            case "next":
                return "Idź do następnej strony możesz użyć strzałek na klawiaturze <span class='glyphicon glyphicon-forward'></span>";
            case "last":
                return "Idź do ostatniej strony <span class='glyphicon glyphicon-fast-forward   '></span>";
            case "page":
                return "Idz do  " + page + " strony";
        }
    }, bootstrapTooltipOptions: {
        html: true,
        placement: 'top'
    }, onPageChanged: function (e, oldPage, newPage) {
        var loc = window.Sites[siteid].url.split(window.Sites[siteid].separator);
        loc[window.Sites[siteid].pageInUrlIndex] = newPage;
        window.Sites[siteid].url = loc.join(window.Sites[siteid].separator);
        window.Sites[siteid].pages.currentPage = newPage;
        call();
    }}
    window.Sites = [
        {
            name: "Kwejk",
            url: "http://kwejk.pl",
            isRevert: true,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<img.+?alt="(.+?)".+?src="(http:\/\/i.+?)".+?\/>/g,
            titleRegexIndex: 1,
            pages: { first: 0, totalPages: 500, currentPage: 0 }
        },
        {
            name: "Komixxy",
            url: "http://komixxy.pl/page/1",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<img.+?alt="(.+?)".+?src="(http:\/\/komixxy.pl\/uimages\/.+?)".+?\/>/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "Mistrzowie",
            url: "http://mistrzowie.org/page/1",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<img.+?alt="(.+?)".+?src="(\/\/mistrzowie.org\/\/uimages.+?)".+?/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "Demotywatory",
            url: "http://demotywatory.pl/page/2",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<img.+?alt="(.+?)".+?src="(http:\/\/img.+?)".+?/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "Wiocha",
            url: "http://www.wiocha.pl/absurdy,1",
            isRevert: false,
            separator: ",",
            pageInUrlIndex: 1,
            regex: /<img.+?alt="(.+?)".+?src="(http:\/\/img.wiocha.pl\/images.+?)".+?/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "Jebzdzidy",
            url: "http://jebzdzidy.pl/strona/1",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<a.+?>(.+?)<\/a.+?\s+<\/div>\s.+\s.+\s+<img.+?src="(http:\/\/img.myepicwall.com.+?)"/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "The Coding love",
            url: "http://thecodinglove.com/page/1",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<a.+?>(.+?)<\/a.+?\s<div class="bodytype">\s<p class="c1"><img src="(http:\/\/.+?)"/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        },
        {
            name: "QA Reactions",
            url: "http://qareacts.tumblr.com/page/1",
            isRevert: false,
            separator: "/",
            pageInUrlIndex: 4,
            regex: /<a.+?>(.+?)<\/a.+?\s<p><img alt="" src="(http:\/\/.+?)"/g,
            titleRegexIndex: 1,
            pages: { first: 1, totalPages: 500, currentPage: 1}
        }
    ];
    $(document).ajaxStart(function () {
        Pace.restart(); // stop
    });
    var siteid = 0;
    call();


    $('.nav.navbar-nav li a').click(function () {
        siteid = $(this).attr('id');
        $('#pagination').empty();
        call();

    });

    if (window.mobile) {
        Hammer(document.body,{}).on("dragleft dragright swipeleft swiperight", function (ev) {
            ev.gesture.preventDefault();
            if (ev.type == 'dragleft' || ev.type == 'dragright') {
                return;
            }
            if (ev.type == 'swipeleft') {
                next();
            } else {
                prev();
            }
        });
    } else {
        $(document).keydown(function (e) {
            if (e.keyCode == 37) { // left
                prev();
            }
            else if (e.keyCode == 39) { // right
                next();
            }
        });
    }

    function call() {
        if (document.getElementById("tester") == undefined && !window.mobile) {
            $('.ad').empty().append($('<img src="img/noadblock.png">'));
            Pace.stop();
            return;
        }

        $('#container').empty();
        $('#soruce').text(window.Sites[siteid].url).attr("href", window.Sites[siteid].url);
        $.ajax({
            url: window.Sites[siteid].url,
            type: 'GET',
            success: function (res) {
                var m;
                var rex = window.Sites[siteid].regex
                while (m = rex.exec(res.responseText)) {
                    var cardTemplate = $("#cardTemplate").html();
                    var cardTemplateGalery = $("#cardTemplateGalery").html();
                    var template;
                    if(m[2].lastIndexOf('?')!=-1)
                    m[2] = m[2].slice(0, m[2].lastIndexOf("?"));
                    if (m[window.Sites[siteid].titleRegexIndex].endsWith('_original')) {
                        template = cardTemplateGalery.format(m[window.Sites[siteid].titleRegexIndex], window.Sites[siteid].url, m[2]);
                    }
                    else {
                        template = cardTemplate.format(m[window.Sites[siteid].titleRegexIndex], window.Sites[siteid].name, m[2]);
                    }

                    $("#container").append(template);
                    // Activate lazy load
                }
                $("img").lazyload({
                    threshold: 300,
                    effect: "fadeIn",
                    failure_limit: 10
                });

                if (window.Sites[siteid].isRevert) {
                    if (window.Sites[siteid].url.endsWith('.pl')) {
                        GetNextPage(res.responseText)
                    }
                }
                $('#pagination').bootstrapPaginator($.extend(window.PagOpt, window.Sites[siteid].pages));
                Pace.stop();
            }
        });
    }

    function GetNextPage(sitestring) {
        rex = /<div class="next_page_wf">.?\s*<a href="(.+?)"/g;
        while (m = rex.exec(sitestring)) {
            var temp = m[1].split(window.Sites[siteid].separator)
            temp[2]--;
            window.Sites[siteid].pages.totalPages = temp[2];
            window.Sites[siteid].pages.currentPage = window.Sites[siteid].pages.totalPages;
            window.Sites[siteid].url = window.Sites[siteid].url + m[1];
        }
    };
    function next() {
        $('#pagination').bootstrapPaginator("showNext")
    }

    function prev() {
        $('#pagination').bootstrapPaginator("showPrevious");
    }

    //    google anal
    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
//    google anal
    //adds
    var likemsg = 'Jeśli podoba Ci się ta strona :';
    var tellmsg = 'Opowiedz o nas znajomym na:';
    addthis.bar.initialize({'default': {
        "backgroundColor": "#333333",
        "buttonColor": "#428BCA",
        "textColor": "#FFFFFF",
        "buttonTextColor": "#fff",
        "hideAfter": 20
    }, rules: [
        {
            "name": "Twitter",
            "match": {
                "referringService": "twitter"
            },
            "message": likemsg,
            "action": {
                "type": "button",
                "text": "Tweetnij!",
                "verb": "share",
                "service": "twitter"
            }
        },
        {
            "name": "Facebook",
            "match": {
                "referringService": "facebook"
            },
            "message": tellmsg,
            "action": {
                "type": "button",
                "text": "Facebooku",
                "verb": "share",
                "service": "facebook"
            }
        },
        {
            "name": "Google",
            "match": {
                "referrer": "google.com"
            },
            "message": "Jeśli podoba Ci się ta strona daj nam:",
            "action": {
                "type": "button",
                "text": "+1",
                "verb": "share",
                "service": "google_plusone_share"
            }
        },
        {
            "name": "blip",
            "match": {
                "referringService": "blip"
            },
            "message": likemsg,
            "action": {
                "type": "button",
                "text": "Post on Blip",
                "verb": "share",
                "service": "blip"
            }
        },
        {
            "name": "gg",
            "match": {
                "referringService": "gg"
            },
            "message": tellmsg,
            "action": {
                "type": "button",
                "text": "GG",
                "verb": "share",
                "service": "gg"
            }
        },
        {
            "name": "linkedin",
            "match": {
                "referringService": "linkedin"
            },
            "message": tellmsg,
            "action": {
                "type": "button",
                "text": "LinkedIn",
                "verb": "share",
                "service": "linkedin"
            }
        },
        {
            "name": "wykop",
            "match": {
                "referringService": "wykop"
            },
            "message": likemsg,
            "action": {
                "type": "button",
                "text": "Wykop ją",
                "verb": "share",
                "service": "wykop"
            }
        },
        {
            "name": "AnyOther",
            "match": {},
            "message": likemsg,
            "action": {
                "type": "button",
                "text": "Podziel się nią",
                "verb": "share",
                "service": "preferred"
            }
        }
    ]});
    //adds
}());
