/**
 * Created by Grzegorz on 03.03.14.
 */
// Activates the Carousel
(function () {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
    window.action = '';
    window.calback = true;
    window.paceOptions = {
        startOnPageLoad: false
    };
    $(document).ajaxStart(function () {
        Pace.restart(); // stop
    });

    var site = "Kwejk";
    var urlKwejk = 'http://kwejk.pl';
    var urlKomixy = 'http://komixxy.pl/page/1';
    var urlMistrzowie = 'http://mistrzowie.org/page/1';
    var tempKwejk;
    call();

    $('.carousel').carousel({
        interval: 5000
    })
    $(document).keydown(function (e) {
        if (e.keyCode == 37) { // left
            next();
        }
        else if (e.keyCode == 39) { // right
            prev();
        }
    });

    $('.left.navi').click(function () {
        window.action = 'next';
        $("#Dragend").dragend({
            scrollToPage: 1

        });
    });
    $('.right.navi').click(function () {
        window.action = 'prev';
        $("#Dragend").dragend({
            scrollToPage: 3

        });

    });
    $('.nav.navbar-nav li a').click(function () {
        site = $(this).attr('id');
        call();

    });

    $("#Dragend").dragend({
        afterInitialize: function () {
            $("#demo").css("visibility", "visible");
        },
        onSwipeEnd: function () {
            if (window.action != '') {
                var act = window.action;
                window.action = '';
                switch (act) {
                    case 'next':
                        next();
                        break;
                    case 'prev':
                        prev();
                        break;
                }
            }
            else {
                if (window.calback) {
                    window.calback = false;
                }
                else {
                    if (this.page == 0) {
                        next();
                        console.log('called next')
                    }

                    else {
                        prev();
                        console.log('called prev')
                    }
                    console.dir(this)
                    console.dir(arguments)
                }
            }

        }
        //preventDrag:true
    });

    function call() {
        $('#container').empty();
        var url;//console.log(urlKwejk);
        switch (site) {
            case "Kwejk":
                url = urlKwejk;
                rex = /<img.+?alt="(.+?)".+?src="(http:\/\/i.+?)".+?\/>/g;
                break;
            case "Komixxy":
                url = urlKomixy;
                rex = /<img.+?alt="(.+?)".+?src="(http:\/\/komixxy.pl\/uimages\/.+?)".+?\/>/g;
                break;
            case "Mistrzowie":
                url = urlMistrzowie;
                rex = /<img.+?alt="(.+?)".+?src="(http:\/\/mistrzowie.org\/\/uimages.+?)".+?/g;
                break;
        }
        //console.log(url);

        $.ajax({
            url: url,
            type: 'GET',
            success: function (res) {
                var m;
                // old one rex = /<img.+?alt="(.+?)".+?src="(.+?)".+?\/>/g;
                //<a href="(.+?)".+?\/>
                while (m = rex.exec(res.responseText)) {
                    var cardTemplate = $("#cardTemplate").html();
                    var template = cardTemplate.format(m[1], site, m[2]);
                    $("#container").append(template);
                    // Activate lazy load
                }
                window.calback = true;
                $("#Dragend").dragend({
                    scrollToPage: 2

                });
                console.log('scrolled to main')
                $("img").lazyload({
                    threshold: 300,
                    effect: "fadeIn",
                    failure_limit: 10
                });

                if (site == "Kwejk") {
                    if (urlKwejk == "http://kwejk.pl") {
                        GetKwejkPage(res.responseText)
                    }
                }

                Pace.stop();
            }
        });


    }

    function GetKwejkPage(sitestring) {
        rex = /<div class="next_page_wf">.?\s*<a href="(.+?)"/g;
        while (m = rex.exec(sitestring)) {
            tempKwejk = urlKwejk + m[1];
        }
    };
    function next() {
        switch (site) {
            case "Kwejk":
                if (urlKwejk == 'http://kwejk.pl')
                    return;
                var loc = urlKwejk.split('/');
                loc[4]++
                urlKwejk = loc.join('/');
                break;
            case "Komixxy":
                var loc = urlKomixy.split('/');
                if (loc[4] <= 1)
                    return;
                loc[4]--
                urlKomixy = loc.join('/')
                break;
            case "Mistrzowie":
                var loc = urlMistrzowie.split('/');
                if (loc[4] <= 1)
                    return;
                loc[4]--
                urlMistrzowie = loc.join('/')
                break;
        }
        call();
    }

    function prev() {
        switch (site) {
            case "Kwejk":
                var loc = urlKwejk.split('/');
                if (urlKwejk == 'http://kwejk.pl') {
                    urlKwejk = tempKwejk;
                    call();
                    return;
                }
                loc[4]--;
                urlKwejk = loc.join('/');
                break;
            case "Komixxy":
                var loc = urlKomixy.split('/');
                loc[4]++
                urlKomixy = loc.join('/');
                break;
            case "Mistrzowie":
                var loc = urlMistrzowie.split('/');
                loc[4]++
                urlMistrzowie = loc.join('/');
                break
        }
        call();
    }
}());
